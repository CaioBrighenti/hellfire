print('CoreLuaLoaded')

function CheckIfFake()
    if thisEntity:IsHero() then
        local ply = thisEntity:GetPlayerID()
        if PlayerResource:IsFakeClient( ply ) then
            --AI_Init() --Keep this on unless you want AI ON
        end
    end
end

--[[
local ABILITY_arrow = thisEntity:FindAbilityByName( 'arrow' )
local ABILITY_blink = thisEntity:FindAbilityByName( 'blink' )
local ABILITY_force_field = thisEntity:FindAbilityByName( 'force_field' )
]]

-- Ability Grenade
-- Ability Laser
-- Ability Flag Throw

function AI_Init()
    thisEntity:SetContextThink('AI_Think', AI_Think, 0.5)
end

function AI_Think()
    print(thisEntity:GetPlayerID())

    local closeEnemies = FindUnitsInRadius( thisEntity:GetTeam(), thisEntity:GetOrigin(), nil, 1300, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NOT_CREEP_HERO, FIND_CLOSEST, false )
    if closeEnemies[1] then
        local aimAttempt = leadArrowPos( closeEnemies[1] )
        fanArrows( aimAttempt, closeEnemies[1]:GetForwardVector() )
    end

    return 0.5
end

function fireArrowPos( vPos )
    local ABILITY_arrow = thisEntity:FindAbilityByName( 'arrow' )
    ExecuteOrderFromTable({
        UnitIndex = thisEntity:entindex(),
        OrderType = DOTA_UNIT_ORDER_CAST_POSITION,
        AbilityIndex = ABILITY_arrow:entindex(),
        Position = vPos,
        Queue = true
        })
end

function fanArrows( vPos, vForwardVector )
    local spreadVector = vForwardVector
    local points = { vPos - (spreadVector * 600 ), vPos - (spreadVector * 300 ), vPos, vPos + (spreadVector * 300 ), vPos + (spreadVector * 600 )}
    for i,v in ipairs(points) do
        fireArrowPos( v )
    end
end

function leadArrowPos( unit )
    local travelTime = (CalculateDistance(unit:GetOrigin(), thisEntity:GetOrigin()) / 900) + 0.1
    local aimPos = unit:GetOrigin() + ((unit:GetForwardVector() * unit:GetBaseMoveSpeed() ) * travelTime)

    print(aimPos)
    print(unit:GetOrigin())
    return aimPos
end

function CalculateDistance(vPoint1, vPoint2)
    local numbers = vPoint1 - vPoint2
    numbers.x = numbers.x^2
    numbers.y = numbers.y^2

    local distance = math.sqrt(numbers.x + numbers.y)
    print(distance)
    return distance
end

CheckIfFake()
