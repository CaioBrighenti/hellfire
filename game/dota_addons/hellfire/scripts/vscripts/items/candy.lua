function Equip( keys )
    --TODO Run Equiped Event
    local ability = keys.ability
    local itemName = tostring(keys.ability:GetAbilityName())

    if itemName == 'item_candy' then
        if ability:GetPurchaser():GetTeam() ~= keys.caster:GetTeam() then
            headhunter:IncrementScore( keys.caster:GetTeam(), 1)
            keys.caster:IncrementAssists(keys.caster:GetPlayerID())
        end
            keys.caster:RemoveItem(ability)
    end
end
