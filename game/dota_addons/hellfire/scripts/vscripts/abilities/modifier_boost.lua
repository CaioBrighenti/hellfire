modifier_boost = class({})

modifier_boost.bonusSpeed = 0
modifier_boost.maxstacks = 5
modifier_boost.usedUnitList = {}
DodgeCount = {}

function modifier_boost:OnCreated( params )
    self:StartIntervalThink( 3 )
end

function modifier_boost:OnIntervalThink()
    if IsServer() then
        modifier_boost.bonusSpeed = self:GetStackCount() * 20

        if self:GetStackCount() ~= 0 then
            self:SetStackCount( self:GetStackCount() - 1 )
        else
            self:SetStackCount( 0 )
        end

        local hCaster = self:GetCaster()
    end
end

function modifier_boost:IncrementStacks( unit )
    local matchunit = false
    for i,v in ipairs(modifier_boost.usedUnitList) do
        if v == unit then
            matchunit = true
        end
    end

    if matchunit == false then
        table.insert(modifier_boost.usedUnitList, unit)
        if self:GetStackCount() < 5 then
            self:IncrementStackCount()
        else
            self:SetStackCount( 5 )
        end
        self:ForceRefresh()

        local vUnit = self:GetCaster():GetOrigin()
        local vUnit = Vector(vUnit.x, vUnit.y, vUnit.z + 500)

        modifier_boost:IncrementDodges( self:GetCaster() )

        EmitSoundOn( "Hero_EmberSpirit.PreAttack.Flame", self:GetCaster())
    end
end

function modifier_boost:SetStacks( iAmount )
    self:SetStackCount( iAmount )
    self:ForceRefresh()
end

function modifier_boost:DeclareFunctions()
    local funcs = {
        MODIFIER_PROPERTY_MOVESPEED_BONUS_CONSTANT,
    }

    return funcs
end

function modifier_boost:GetModifierMoveSpeedBonus_Constant( params )
        return self.bonusSpeed
end

function modifier_boost:IncrementDodges( hCaster )
    local ply = hCaster:GetOwner():GetPlayerID()
    if DodgeCount[ply] == nil then
        DodgeCount[ply] = 0
    end

    DodgeCount[ply] = DodgeCount[ply] + 1
    CustomNetTables:SetTableValue( "HF_PlayerData", tostring(ply), { dodges = DodgeCount[ply] })
end
