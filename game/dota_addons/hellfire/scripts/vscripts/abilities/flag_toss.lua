flag_toss = class({})

function flag_toss:GetCastAnimation()
    return ACT_DOTA_CAST_ABILITY_1
end

function flag_toss:OnAbilityPhaseStart()
        local hCaster = self:GetCaster()
        local vPoint = self:GetCursorPosition()
        local fMaxRange = self:GetSpecialValueFor("MaxRange")
        local vAttemptPoint = vPoint

        local inventory = {hCaster:GetItemInSlot(0),
        hCaster:GetItemInSlot(1),
        hCaster:GetItemInSlot(2),
        hCaster:GetItemInSlot(3),
        hCaster:GetItemInSlot(4),
        hCaster:GetItemInSlot(5)
    }

    if flag_toss:CalculateDistance( hCaster, vPoint) > 500 then
        vAttemptPoint = flag_toss:CalculateMaxRangeLoc( hCaster, vPoint, fMaxRange )
    end

    if GridNav:CanFindPath( hCaster:GetOrigin(), vAttemptPoint ) ~= true or GridNav:IsTraversable( vAttemptPoint ) ~= true then
        return false
    end

    for i, v in pairs(inventory) do
        if v ~= nil then
            print(v:GetName())
            if v:GetName() == "item_capture_flag_radiant" or v:GetName() == "item_capture_flag_dire" then
                return true
            end
        end
    end

    return false
end

function flag_toss:OnSpellStart()
    local hCaster = self:GetCaster()
    local vPoint = self:GetCursorPosition()
    local fMaxRange = self:GetSpecialValueFor("MaxRange")
    local fSpeed = self:GetSpecialValueFor("Velocity")
    local fHeight = self:GetSpecialValueFor("Height")

    local inventory = {hCaster:GetItemInSlot(0),
        hCaster:GetItemInSlot(1),
        hCaster:GetItemInSlot(2),
        hCaster:GetItemInSlot(3),
        hCaster:GetItemInSlot(4),
        hCaster:GetItemInSlot(5)
        }

    for i, v in pairs(inventory) do
        if v ~= nil then
            print(v:GetName())
            if v:GetName() == "item_capture_flag_radiant" or v:GetName() == "item_capture_flag_dire" then
                local newItem = CreateItem(v:GetName(), nil, nil)
                newItem:SetPurchaseTime( 0 )
                local drop = CreateItemOnPositionSync( hCaster:GetOrigin() , newItem)
                hCaster:RemoveItem(v)
                if flag_toss:CalculateDistance( hCaster, vPoint) > fMaxRange then
                    vPoint = flag_toss:CalculateMaxRangeLoc( hCaster, vPoint, fMaxRange )
                end

                newItem:LaunchLoot(false, 300, 0.75, vPoint)
            end
        end
    end
end

function flag_toss:CalculateDistance(hCaster, vPoint)
    local vCaster = hCaster:GetOrigin()

    local numbers = vPoint - vCaster
    numbers.x = numbers.x^2
    numbers.y = numbers.y^2

    local distance = math.sqrt(numbers.x + numbers.y)
    return distance
end

function flag_toss:CalculateMaxRangeLoc(hCaster, vPoint, iMaxRange)
    local vCaster = hCaster:GetOrigin()

    vPoint = Vector(vPoint.x, vPoint.y, 0)
    vCaster = Vector(vCaster.x, vCaster.y, 0)
    local vFiringVector = vPoint - vCaster
    vFiringVector = vFiringVector:Normalized()

    return vCaster + vFiringVector * iMaxRange
end
