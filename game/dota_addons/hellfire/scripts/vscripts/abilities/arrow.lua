arrow = class({})

ShotsFired = {}

function arrow:GetCastAnimation()
        return ACT_DOTA_CAST_ABILITY_2
end

function arrow:GetPlaybackRateOverride()
    return 5
end

function arrow:OnAbilityPhaseStart()
    local hCaster = self:GetCaster()

    return true
end

function arrow:OnAbilityPhaseInterrupted()
    local hCaster = self:GetCaster()
end

function arrow:OnSpellStart()
    local hCaster = self:GetCaster()
    local vPoint = self:GetCursorPosition()
    local vCaster = hCaster:GetAbsOrigin()
    local iVelocity = self:GetSpecialValueFor("Velocity")
    local ability = self
    local vColor = Vector(0,0,0)

    if HellfireGameMode.Colors then
            vColor.x = HellfireGameMode.Colors[hCaster:GetTeam()][1]
            vColor.y = HellfireGameMode.Colors[hCaster:GetTeam()][2]
            vColor.z = HellfireGameMode.Colors[hCaster:GetTeam()][3]
    end

    vPoint = Vector(vPoint.x, vPoint.y, 0)
    vCaster = Vector(vCaster.x, vCaster.y, 0)
    local vFiringVector = vPoint - vCaster
    vFiringVector = vFiringVector:Normalized()

    local projectile = {
        EffectName = "particles/arrow/arrow.vpcf",
        ControlPoints = {[4] = vColor},
        vSpawnOrigin = hCaster:GetAbsOrigin() + Vector(0,0,105),
        fDistance = 9000,
        fStartRadius = 150,
        fEndRadius = 150,
        Source = hCaster,
        fExpireTime = 10.0,
        vVelocity = vFiringVector * iVelocity, -- RandomVector(1000),
        UnitBehavior = PROJECTILES_DESTROY,
        bMultipleHits = false,
        bIgnoreSource = true,
        TreeBehavior = PROJECTILES_NOTHING,
        bCutTrees = true,
        bTreeFullCollision = false,
        WallBehavior = PROJECTILES_DESTROY,
        GroundBehavior = PROJECTILES_DESTROY,
        fGroundOffset = 80,
        nChangeMax = 1,
        bRecreateOnChange = true,
        bZCheck = true,
        bGroundLock = false,
        bProvidesVision = true,
        iVisionRadius = 350,
        iVisionTeamNumber = hCaster:GetTeam(),
        bFlyingVision = false,
        fVisionTickTime = .1,
        fVisionLingerDuration = 1,
        draw = false,

        UnitTest = function(self, unit)
                if unit:GetUnitName() ~= "npc_dummy_unit" and unit:GetTeamNumber() ~= hCaster:GetTeamNumber() then
                    if unit:IsHero() then
                        --TODO add Boosting Mechanic Here DODGE
                        -- Make sure to check source to see if this arrow has already given a boost to this hero or not.
                        local modifier = unit:FindModifierByName('modifier_boost')

                        if modifier then
                            modifier:IncrementStacks( self )
                        end
                    end
                    if arrow:CalculateDistance(self.pos, unit:GetOrigin()) < 85 then
                        return true
                    end
                end

                if unit:GetUnitName() == "npc_force_field" then
                    return true
                end
                return false
            end,

        OnUnitHit = function(self, unit)

                if unit:GetUnitName() ~= "npc_force_field" then
                    local damage = {
                        victim = unit,
                        attacker = hCaster,
                        damage = ability:GetSpecialValueFor("Damage"),
                        damage_type = DAMAGE_TYPE_PURE,
                        ability = ability
                    }

                    ApplyDamage( damage )
                end

                EmitSoundOn( "Hero_Windrunner.ProjectileImpact", unit)
            end,
            --OnTreeHit = function(self, tree) ... end,
            OnWallHit = function(self, gnvPos)
                EmitSoundOnLocationWithCaster( gnvPos, "Hero_Windrunner.ProjectileImpact", hCaster)
                end,
            OnGroundHit = function(self, groundPos)
                EmitSoundOnLocationWithCaster( groundPos, "Hero_Windrunner.ProjectileImpact", hCaster)
                end,

            OnFinish = function(self, pos)
            end
        }

        Projectiles:CreateProjectile(projectile)
        arrow:IncrementShotsFired( hCaster )
        EmitSoundOn( "Hero_Windrunner.Attack", hCaster)
end

function arrow:CalculateDistance(vPoint1, vPoint2)
    local numbers = vPoint1 - vPoint2
    numbers.x = numbers.x^2
    numbers.y = numbers.y^2

    local distance = math.sqrt(numbers.x + numbers.y)
    return distance
end

function arrow:IncrementShotsFired( hCaster )
    local ply = hCaster:GetOwner():GetPlayerID()
    if ShotsFired[ply] == nil then
        ShotsFired[ply] = 0
    end

    ShotsFired[ply] = ShotsFired[ply] + 1
    CustomNetTables:SetTableValue( "HF_PlayerData", tostring(ply), { shots = ShotsFired[ply] })
end
