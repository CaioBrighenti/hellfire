blink = class({})

function blink:GetCastAnimation()
    return ACT_DOTA_CAST_ABILITY_1
end

function blink:OnSpellStart()
    local hCaster = self:GetCaster()
    local vPoint = self:GetCursorPosition()
    local iMaxRange = self:GetSpecialValueFor("MaxRange")

    if blink:CalculateDistance(hCaster, vPoint) > self:GetSpecialValueFor("MaxRange") then
        vPoint = blink:CalculateMaxRangeLoc(hCaster, vPoint, iMaxRange)
    end

    local particle = ParticleManager:CreateParticle("particles/items_fx/blink_dagger_start.vpcf", PATTACH_ABSORIGIN , hCaster)
    ParticleManager:SetParticleControl(particle, 0, GetGroundPosition(hCaster:GetOrigin(), hCaster))
    ParticleManager:ReleaseParticleIndex(particle)

    blink:CalculateDistance(hCaster, vPoint)
    FindClearSpaceForUnit(hCaster, vPoint, true)
    local particle = ParticleManager:CreateParticle("particles/items_fx/blink_dagger_end.vpcf", PATTACH_ABSORIGIN_FOLLOW , hCaster)
    ParticleManager:SetParticleControl(particle, 0, GetGroundPosition(hCaster:GetOrigin(), hCaster))
    ParticleManager:ReleaseParticleIndex(particle)

    EmitSoundOn( "DOTA_Item.BlinkDagger.Activate", hCaster )

end

function blink:CalculateDistance(hCaster, vPoint)
    local vCaster = hCaster:GetOrigin()

    local numbers = vPoint - vCaster
    numbers.x = numbers.x^2
    numbers.y = numbers.y^2

    local distance = math.sqrt(numbers.x + numbers.y)
    return distance
end

function blink:CalculateMaxRangeLoc(hCaster, vPoint, iMaxRange)
    local vCaster = hCaster:GetOrigin()

    vPoint = Vector(vPoint.x, vPoint.y, 0)
    vCaster = Vector(vCaster.x, vCaster.y, 0)
    local vFiringVector = vPoint - vCaster
    vFiringVector = vFiringVector:Normalized()

    return vCaster + vFiringVector * iMaxRange
end
