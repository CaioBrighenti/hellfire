laser = class({})

function laser:GetCastAnimation()
    return ACT_DOTA_CAST_ABILITY_2
end

function laser:GetPlaybackRateOverride()
    return 5
end

local vEndPos = {}

function laser:OnAbilityPhaseStart()

    local hero = self:GetCaster()
    local vCaster = hero:GetAbsOrigin()
    local vPoint = self:GetCursorPosition()

    vPoint = Vector(vPoint.x, vPoint.y, 0)
    vCaster = Vector(vCaster.x, vCaster.y, 0)
    local vFiringVector = vPoint - vCaster
    vFiringVector = vFiringVector:Normalized()

    local projectile = {
        --EffectName = "particles/test_particle/ranged_tower_good.vpcf",
        --EffectName = "particles/units/heroes/hero_lina/lina_spell_dragon_slave.vpcf",
        EffectName = "particles/units/heroes/hero_mirana/mirana_spell_arrow.vpcf",
        --EeffectName = "",
        --vSpawnOrigin = hero:GetAbsOrigin(),
        vSpawnOrigin = hero:GetAbsOrigin() + Vector(0,0,80),--{unit=hero, attach="attach_attack1", offset=Vector(0,0,0)},
        fDistance = 2000,
        fStartRadius = 100,
        fEndRadius = 100,
        Source = hero,
        fExpireTime = 8.0,
        vVelocity = vFiringVector * 10000, -- RandomVector(1000),
        UnitBehavior = PROJECTILES_NOTHING,
        bMultipleHits = false,
        bIgnoreSource = true,
        TreeBehavior = PROJECTILES_NOTHING,
        bCutTrees = true,
        bTreeFullCollision = false,
        WallBehavior = PROJECTILES_DESTROY,
        GroundBehavior = PROJECTILES_DESTROY,
        fGroundOffset = 80,
        nChangeMax = 1,
        bRecreateOnChange = true,
        bZCheck = false,
        bGroundLock = false,
        bProvidesVision = true,
        iVisionRadius = 350,
        iVisionTeamNumber = hero:GetTeam(),
        bFlyingVision = false,
        fVisionTickTime = .1,
        fVisionLingerDuration = 1,
        draw = false,--             draw = {alpha=1, color=Vector(200,0,0)},
        --iPositionCP = 0,
        --iVelocityCP = 1,
        --ControlPoints = {[5]=Vector(100,0,0), [10]=Vector(0,0,1)},
        --ControlPointForwards = {[4]=hero:GetForwardVector() * -1},
        --ControlPointOrientations = {[1]={hero:GetForwardVector() * -1, hero:GetForwardVector() * -1, hero:GetForwardVector() * -1}},
        --[[ControlPointEntityAttaches = {[0]={
        unit = hero,
        pattach = PATTACH_ABSORIGIN_FOLLOW,
        attachPoint = "attach_attack1", -- nil
        origin = Vector(0,0,0)
        }},]]
        --fRehitDelay = .3,
        --fChangeDelay = 1,
        --fRadiusStep = 10,
        --bUseFindUnitsInRadius = false,

        UnitTest = function(self, unit) return unit:GetUnitName() ~= "npc_dummy_unit" and unit:GetTeamNumber() ~= hero:GetTeamNumber() end,
        OnUnitHit = function(self, unit)
            end,
            --OnTreeHit = function(self, tree) ... end,
            OnWallHit = function(self, gnvPos)
                    vEndPos[hero] = gnvPos
                end,
            OnGroundHit = function(self, groundPos)
                    vEndPos[hero] = gnvPos
                end,
            OnFinish = function(self, pos)
                    vEndPos[hero] = pos
                end,
        }

        Projectiles:CreateProjectile(projectile)

    return true
end

function laser:OnAbilityPhaseInterrupted()
    local hCaster = self:GetCaster()
end

function laser:OnSpellStart()
    local hCaster = self:GetCaster()
    local vCaster = hCaster:GetOrigin()
    local vColor = Vector(0,0,0)

    if HellfireGameMode.Colors then
        vColor.x = HellfireGameMode.Colors[hCaster:GetTeam()][1]
        vColor.y = HellfireGameMode.Colors[hCaster:GetTeam()][2]
        vColor.z = HellfireGameMode.Colors[hCaster:GetTeam()][3]
    end

    local particle = ParticleManager:CreateParticle("particles/laser/laser_beam.vpcf", PATTACH_CUSTOMORIGIN, hCaster)
    local zPos = GetGroundPosition(vCaster, hCaster).z + 100
    ParticleManager:SetParticleControl(particle, 0, Vector( vCaster.x, vCaster.y, zPos ) )
    ParticleManager:SetParticleControl(particle, 3, Vector( vEndPos[hCaster].x, vEndPos[hCaster].y, zPos ) )
    ParticleManager:SetParticleControl(particle, 4, Vector( vColor.x, vColor.y, vColor.z ) )
    ParticleManager:ReleaseParticleIndex( particle )

end

function laser:OnChannelFinish( bInterrupted )
    if bInterrupted == false then
        laser:CreateLaser(self:GetCaster(), vEndPos[self:GetCaster()])
    end
end

function laser:CreateLaser(hero, vPoint)

    local hCaster = hero
    local vCaster = hero:GetAbsOrigin()
    local vPoint = vEndPos[hCaster]
    local vColor = Vector(0,0,0)

    if HellfireGameMode.Colors then
        vColor.x = HellfireGameMode.Colors[hCaster:GetTeam()][1]
        vColor.y = HellfireGameMode.Colors[hCaster:GetTeam()][2]
        vColor.z = HellfireGameMode.Colors[hCaster:GetTeam()][3]
    end

    vPoint = Vector(vPoint.x, vPoint.y, 0)
    vCaster = Vector(vCaster.x, vCaster.y, 0)
    local vFiringVector = vPoint - vCaster
    vFiringVector = vFiringVector:Normalized()

    local vShotDistance = laser:CalculateDistance(vCaster, vPoint)

    for i = 0, 20, 1 do

        local vOffset = hero:GetAbsOrigin() + ( vFiringVector * (( vShotDistance / 20 ) * i ) ) --Creates 20 laser projectiles, using I as the offset.
        local vShotDistanceNew = laser:CalculateDistance(vOffset, vPoint)

        local projectile = {
            --EffectName = "particles/test_particle/ranged_tower_good.vpcf",
            --EffectName = "particles/units/heroes/hero_lina/lina_spell_dragon_slave.vpcf",
            EffectName = "particles/units/heroes/hero_mirana/mirana_spell_arrow.vpcf",
            --EeffectName = "",
            --vSpawnOrigin = hero:GetAbsOrigin(),
            vSpawnOrigin = vOffset,--{unit=hero, attach="attach_attack1", offset=Vector(0,0,0)},
            fDistance = vShotDistanceNew,
            fStartRadius = 100,
            fEndRadius = 100,
            Source = hero,
            fExpireTime = .35,
            vVelocity = vFiringVector * 1, -- A Speed of zero won't hit anythng so leave this at 1.
            UnitBehavior = PROJECTILES_NOTHING,
            bMultipleHits = true,
            bIgnoreSource = true,
            TreeBehavior = PROJECTILES_NOTHING,
            bCutTrees = true,
            bTreeFullCollision = false,
            WallBehavior = PROJECTILES_NOTHING,
            GroundBehavior = PROJECTILES_NOTHING,
            fGroundOffset = 80,
            nChangeMax = 1,
            bRecreateOnChange = true,
            bZCheck = false,
            bGroundLock = false,
            bProvidesVision = true,
            iVisionRadius = 120,
            iVisionTeamNumber = hero:GetTeam(),
            bFlyingVision = false,
            fVisionTickTime = .1,
            fVisionLingerDuration = 1,
            draw = false,--             draw = {alpha=1, color=Vector(200,0,0)},
            --iPositionCP = 0,
            --iVelocityCP = 1,
            --ControlPoints = {[5]=Vector(100,0,0), [10]=Vector(0,0,1)},
            --ControlPointForwards = {[4]=hero:GetForwardVector() * -1},
            --ControlPointOrientations = {[1]={hero:GetForwardVector() * -1, hero:GetForwardVector() * -1, hero:GetForwardVector() * -1}},
            --[[ControlPointEntityAttaches = {[0]={
            unit = hero,
            pattach = PATTACH_ABSORIGIN_FOLLOW,
            attachPoint = "attach_attack1", -- nil
            origin = Vector(0,0,0)
            }},]]
            --fRehitDelay = .3,
            --fChangeDelay = 1,
            --fRadiusStep = 10,
            --bUseFindUnitsInRadius = false,

            UnitTest = function(self, unit) return unit:GetUnitName() ~= "npc_dummy_unit" and unit:GetTeamNumber() ~= hero:GetTeamNumber() end,
            OnUnitHit = function(self, unit)
                    if unit:GetUnitName() ~= "npc_force_field" then
                        local damage = {
                            victim = unit,
                            attacker = hCaster,
                            damage = 9001,
                            damage_type = DAMAGE_TYPE_PURE,
                            ability = ability
                        }

                        ApplyDamage( damage )
                    end
                end,
                --OnTreeHit = function(self, tree) ... end,
                --OnWallHit = function(self, gnvPos) ... end,
                --OnGroundHit = function(self, groundPos) ... end,
                --OnFinish = function(self, pos) ... end,
            }

            Projectiles:CreateProjectile(projectile)
        end

        local particle = ParticleManager:CreateParticle("particles/laser/laser_fire.vpcf", PATTACH_CUSTOMORIGIN, hero)
        local zPos = GetGroundPosition(vCaster, hero).z + 100
        ParticleManager:SetParticleControl(particle, 0, Vector( vCaster.x, vCaster.y, zPos ) )
        ParticleManager:SetParticleControl(particle, 3, Vector( vEndPos[hero].x, vEndPos[hero].y, zPos ) )
        ParticleManager:SetParticleControl(particle, 4, Vector( vColor.x, vColor.y, vColor.z ) )
        ParticleManager:ReleaseParticleIndex( particle )
        --Kobb can't pull
end

function laser:CalculateDistance(vPoint1, vPoint2)
    local numbers = vPoint1 - vPoint2
    numbers.x = numbers.x^2
    numbers.y = numbers.y^2

    local distance = math.sqrt(numbers.x + numbers.y)
    return distance
end
