modifier_force_field = class({})

function modifier_force_field:OnCreated()
    local hCaster = self:GetCaster()
end

function modifier_force_field:CheckState()
        local state = {
            [MODIFIER_STATE_NO_HEALTH_BAR] = true,
            [MODIFIER_STATE_INVULNERABLE] = true,
            [MODIFIER_STATE_NOT_ON_MINIMAP] = true,
            [MODIFIER_STATE_UNSELECTABLE] = true,
        }

        return state
end
