force_field = class({})

function force_field:OnAbilityPhaseStart()
    return true
end

local unitTable = {}

function force_field:OnSpellStart()
    local hCaster = self:GetCaster()
    local vPoint = self:GetCursorPosition()
    local iMaxRange = self:GetSpecialValueFor("MaxRange")
    local iRadius = self:GetSpecialValueFor("Radius")
    local vColor = Vector(255,255,255)

    if HellfireGameMode.Colors then
        vColor.x = HellfireGameMode.Colors[hCaster:GetTeam()][1]
        vColor.y = HellfireGameMode.Colors[hCaster:GetTeam()][2]
        vColor.z = HellfireGameMode.Colors[hCaster:GetTeam()][3]
    end

    force_field:KnockBack( vPoint, 150, 0.3, 155, 20)

    unit = CreateUnitByName("npc_force_field", vPoint, false, nil, hCaster, hCaster:GetTeam())

    unit:AddNewModifier( unit, nil, "modifier_force_field", {duration = -1} )

    unit:SetHullRadius( 150 )

    unitTable[unit] = {
        lifetime = 0,
    }

    local alpha = 1
    local color = Vector(0,0,255)

    -- Visual Reference
    --DebugDrawSphere(vPoint, color, alpha, 150, true, 0.1)
    local particle = ParticleManager:CreateParticle("particles/force_field/force_field_shell.vpcf", PATTACH_ABSORIGIN_FOLLOW , unit)
    ParticleManager:SetParticleControl(particle, 1, Vector(150,0,0))
    ParticleManager:SetParticleControl(particle, 2, vColor)

    --mystic_flare_ambient_i == warp effect
    -- particles/units/heroes/hero_medusa/medusa_mana_shield_oom.vpcf
    -- particles/units/heroes/hero_skywrath_mage/skywrath_mage_mystic_flare_ambient.vpcf


    --ParticleManager:ReleaseParticleIndex(particle)

    unit:SetContextThink( "OnThink", force_field.OnThink, 0.1 )
end

function force_field:OnThink()
        for unit, v in pairs(unitTable) do
            if v.lifetime >= 50 then
                unitTable[unit] = nil
                UTIL_Remove(unit)
            else
                local alpha = 1
                local color = Vector(0,0,255)
                --DebugDrawSphere(unit:GetOrigin(), color, alpha, 150, true, 0.1)
                v.lifetime = v.lifetime + 1
            end
        end

        return 0.1
end

function force_field:KnockBack( center, radius, knockback_duration, knockback_distance, knockback_height )
    local knockBackUnits = FindUnitsInRadius( DOTA_TEAM_NOTEAM, center, nil, radius, DOTA_UNIT_TARGET_TEAM_BOTH, DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false )

    local modifierKnockback =
    {
        center_x = center.x,
        center_y = center.y,
        center_z = center.z,
        duration = knockback_duration,
        knockback_duration = knockback_duration,
        knockback_distance = knockback_distance,
        knockback_height = knockback_height,
    }

    for _,unit in pairs(knockBackUnits) do
        --		print( "knock back unit: " .. unit:GetName() )
        unit:AddNewModifier( unit, nil, "modifier_knockback", modifierKnockback );
    end
end
