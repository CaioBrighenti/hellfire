modifier_player_modifiers = class({})

function modifier_player_modifiers:OnCreated()
        self:StartIntervalThink(0.1)
end

function modifier_player_modifiers:DeclareFunctions()
    local funcs = {
        MODIFIER_EVENT_ON_ABILITY_EXECUTED,
    }

    return funcs
end

function modifier_player_modifiers:CheckState()
        local state = {
            --[MODIFIER_STATE_NO_HEALTH_BAR] = true,
        }

        return state
end

function modifier_player_modifiers:OnIntervalThink()
    local hCaster = self:GetCaster()
    local iRegenDelay = 3 -- Time in seconds till regen starts
    local iRegenPercent = 3 -- percent of mana to regen

        if IsServer() then
            if self:GetStackCount() > (iRegenDelay * 10) then
                hCaster:GiveMana( hCaster:GetMaxMana() * (iRegenPercent / 100) )
            end

            self:IncrementStackCount()
        end
end

function modifier_player_modifiers:OnAbilityExecuted( params )
    if IsServer() then
        if params.unit == self:GetParent() then
            self:SetStackCount( 0 )
        end
    end

    return 0
end
