--------------------------------------------------------------------------------
-- Event: OnNPCSpawned
--------------------------------------------------------------------------------
function HellfireGameMode:OnNPCSpawned( event )
    local spawnedUnit = EntIndexToHScript( event.entindex )
    if spawnedUnit ~= nil then
        if spawnedUnit:IsRealHero() then
            for i = 0, spawnedUnit:GetAbilityCount() do
                ability = spawnedUnit:GetAbilityByIndex(i)
                if ability ~= nil then
                    ability:SetLevel(ability:GetMaxLevel())
                end
            end
        end
        spawnedUnit:AddNewModifier( spawnedUnit, nil, "modifier_player_modifiers", {duration = -1} )
        if spawnedUnit:HasModifier('modifier_boost') == false then
            spawnedUnit:AddNewModifier( spawnedUnit, nil, "modifier_boost", {duration = -1} )
        end
    end
end

function HellfireGameMode:OnItemPickedUp( event )
end

local GameModes = {
            "capturetheflag",
            "kingofthehill",
            "domination",
            "headhunter"
        }
local GameModeVotes = {}
local max = {0,0}

function HellfireGameMode:OnVoteUpdate( event )
    print('ONVOTEUPDATE')
    local player = event.player
    local modeVoted = event.modeVoted
    local extra1Voted = event.extra1Voted
    local extra2Voted = event.extra2Voted
    local extra3Voted = event.extra3Voted

    print(player)
    print(modeVoted)
    print(extra1Voted)
    print(extra2Voted)
    print(extra3Voted)

    if GameModeVotes[modeVoted] == nil then GameModeVotes[modeVoted] = 0 end
    GameModeVotes[modeVoted] = GameModeVotes[modeVoted] + 1
end

function HellfireGameMode:OnStateChange( event )
    print('StateChanged to ' .. GameRules:State_Get())
    if GameRules:State_Get() ==  DOTA_GAMERULES_STATE_STRATEGY_TIME then
        for i,v in pairs(GameModeVotes) do
            if max[2] < v then
                max[1] = i
                max[2] = v
            end
        end

        print(max[1])
        print('Gamemode Vote Winner == ' .. GameModes[max[1]])
        GameModeManager:SetGameMode( GameModes[max[1]] )
    end
end
