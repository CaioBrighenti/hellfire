---------------------------------------------------------------------------
-- Hellfire Gamemode - Mini GameMode HeadHunter
---------------------------------------------------------------------------

if headhunter == nil then
    -- headhunter = the custom gamemode running in the manager
    _G.headhunter = GameModeManager.CustomGameMode
end

function headhunter:Init()
    print('Loaded Headhunter')

    -- CONSTANTS
    SCORE_LIMIT = 25

    -- Variables
    TeamScores = {}
    GAME_WINNER = 2

    -- Setup ThinkState
    headhunter.thinkState = Dynamic_Wrap( headhunter, 'thinkState_PreGame' )

    -- GameRule changes
    GameMode = GameRules:GetGameModeEntity()
    GameMode:SetTopBarTeamValuesOverride( true )

    -- Register Console Commands
    headhunter:RegisterCommands()
        -- HF_SetScore < iTeamNumber > < iScore > - Sets the score to the specified value

    -- Listener
    ListenToGameEvent('entity_killed', Dynamic_Wrap(headhunter, 'OnEntityKilled'), self)
end

-- Called from GameModeManager.lua, which is called from Hellfire.lua Think So all thinks are the same.
function headhunter:think()
        headhunter:thinkState()
end

--Check if game is in pregame, if so do nothing.
function headhunter:thinkState_PreGame()
    if GameRules:State_Get() <= DOTA_GAMERULES_STATE_PRE_GAME then
        return
    end

    headhunter.thinkState = Dynamic_Wrap( headhunter, 'thinkState_InGame' )
end

-- Update and check scores for end game.
function headhunter:thinkState_InGame()

    for i,v in pairs(TeamScores) do
        GameMode:SetTopBarTeamValue( i, v )
        if v > SCORE_LIMIT then
            GAME_WINNER = i
            headhunter.thinkState = Dynamic_Wrap( headhunter, 'thinkState_PostGame' )
        end
    end
end

-- Set the gamemode to postgame, and chill
function headhunter:thinkState_PostGame()

    GameRules:SetGameWinner( GAME_WINNER )
    GameRules:SetSafeToLeave( true )
end

-- Drops a candy when an entity dies.
function headhunter:OnEntityKilled( event )
    local killedEnt = EntIndexToHScript(event.entindex_killed)

    if killedEnt ~= nil then
        local newItem = CreateItem( 'item_candy' , killedEnt, killedEnt)
        newItem:SetPurchaseTime( 0 )
        local groundItem = CreateItemOnPositionSync( killedEnt:GetOrigin() , newItem)

        local vColor = Vector(0,0,0)

        if HellfireGameMode.Colors then
            vColor.x = HellfireGameMode.Colors[killedEnt:GetTeam()][1]
            vColor.y = HellfireGameMode.Colors[killedEnt:GetTeam()][2]
            vColor.z = HellfireGameMode.Colors[killedEnt:GetTeam()][3]
        end

        local particle = ParticleManager:CreateParticle("particles/candy/candy_aura_tintable.vpcf", PATTACH_ROOTBONE_FOLLOW , groundItem)
        ParticleManager:SetParticleControl(particle, 2, vColor)
        ParticleManager:ReleaseParticleIndex( particle )
    end
end

-- Increments the score of a team when a candy is picked up. Called from the candy item.
function headhunter:IncrementScore( iTeam, iAmount )
    if TeamScores[iTeam] == nil then TeamScores[iTeam] = 0 end
    TeamScores[iTeam] = TeamScores[iTeam] + iAmount

    CustomNetTables:SetTableValue( "TeamScore", tostring(iTeam), { score = TeamScores[iTeam] })
    print(CustomNetTables:GetTableValue( "TeamScore", tostring(iTeam)).score )
end

-- Console Commands
function headhunter:RegisterCommands()
    print('Registering Commands')

    Convars:RegisterCommand('HF_SetScore', function( name, iTeam, iScore )
        if TeamScores[tonumber(iTeam)] == nil then TeamScores[tonumber(iTeam)] = 0 end
        TeamScores[tonumber(iTeam)] = tonumber(iScore)
        print('score = ' .. TeamScores[tonumber(iTeam)])
    end, 'Set a teams score as needed', FCVAR_CHEAT)

    Convars:RegisterCommand('HF_SetStacks', function(name, iStacks)

        local ply = Convars:GetCommandClient()
        local unit = ply:GetAssignedHero()
        print(unit:GetName())
        local modifier = unit:FindModifierByName('modifier_boost')
        print(modifier:GetName())
        if modifier then
            modifier:SetStacks( tonumber(iStacks) )
        end

    end, 'Set boost stacks on a unit', FCVAR_CHEAT)
end
