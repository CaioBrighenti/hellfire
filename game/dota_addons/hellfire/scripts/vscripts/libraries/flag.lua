function spawnRadiantFlag( )
    local goodGuysBase = Entities:FindByName(nil, 'flag_goodguys')
    local goodGuysBaseVec = goodGuysBase:GetOrigin()
    local flag = CreateItem('item_capture_flag', nil, nil)
    local flag_drop = CreateItemOnPositionSync(goodGuysBaseVec, flag)
    if flag_drop then
        flag_drop:SetContainedItem( flag )
    end
end

function spawnDireFlag( )
    local badGuysBase = Entities:FindByName(nil, 'flag_badguys')
    local badGuysBaseVec = badGuysBase:GetOrigin()
    local flag = CreateItem('item_capture_flag_dire', nil, nil)
    local flag_drop = CreateItemOnPositionSync(badGuysBaseVec, flag)
    if flag_drop then
        flag_drop:SetContainedItem( flag )
        flag_drop:SetAngles(0,180,0)
    end
end

function HellfireGameMode:UpdateFlags( )
            local goodGuysBase = Entities:FindByName(nil, 'flag_goodguys')
            local goodGuysBaseVec = goodGuysBase:GetOrigin()
            local badGuysBase = Entities:FindByName(nil, 'flag_badguys')
            local badGuysBaseVec = badGuysBase:GetOrigin()
            local goodUnitsOnPoint = FindUnitsInRadius(DOTA_TEAM_GOODGUYS, goodGuysBaseVec, null, 150, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO, 0, 0, false)
            local badUnitsOnPoint = FindUnitsInRadius(DOTA_TEAM_BADGUYS, badGuysBaseVec, null, 150, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO, 0, 0, false)
            local tableSize = 0

            for k,v in pairs(goodUnitsOnPoint) do
                tableSize = tableSize + 1
            end

            for i=1,tableSize do
                local hero = goodUnitsOnPoint[i]
                if hero then
                    for i=0,5 do
                        local item = hero:GetItemInSlot(i)
                        if item then
                            if item:GetAbilityName() == 'item_capture_flag_dire' then
                               UTIL_RemoveImmediate(item)
                               hero:RemoveModifierByName('modifier_creep_slow')
                               heroWithDireFlag = nil
                               local particleFlag = ParticleManager:CreateParticle( 'legion_commander_duel_victory', PATTACH_OVERHEAD_FOLLOW, hero)
                               local particlePlus = ParticleManager:CreateParticle( 'pudge_fleshheap_count', PATTACH_OVERHEAD_FOLLOW, hero)
                               spawnDireFlag()
                               CustomGameEventManager:Send_ServerToPlayer(hero:GetPlayerOwner(), "hellfire_flag_lost", {} )
                            end
                        end
                    end
                end
            end

            tableSize = 0

            for k,v in pairs(badUnitsOnPoint) do
                tableSize = tableSize + 1
            end

            for i=1,tableSize do
                local hero = badUnitsOnPoint[i]
                if hero then
                    for i=0,5 do
                        local item = hero:GetItemInSlot(i)
                        if item then
                            if item:GetAbilityName() == 'item_capture_flag' then
                               UTIL_RemoveImmediate(item)
                               hero:RemoveModifierByName('modifier_creep_slow')
                               heroWithFlag = nil
                               local particleFlag = ParticleManager:CreateParticle( 'legion_commander_duel_victory', PATTACH_OVERHEAD_FOLLOW, hero)
                               local particlePlus = ParticleManager:CreateParticle( 'pudge_fleshheap_count', PATTACH_OVERHEAD_FOLLOW, hero)
                               spawnRadiantFlag()
                               CustomGameEventManager:Send_ServerToPlayer(hero:GetPlayerOwner(), "hellfire_flag_lost", {} )
                            end
                        end
                    end
                end
            end

            if heroWithFlag then
                local hero = heroWithFlag
                if hero then
                    for i=0, 5 do
                        local item = hero:GetItemInSlot(i)
                        if item then
                            if item:GetAbilityName() == 'item_capture_flag' or item:GetAbilityName() == 'item_capture_flag_dire' then
                                hero:AddNewModifier(hero, nil, 'modifier_creep_slow' ,nil)
                                heroWithFlag = hero
                                break
                            else
                                hero:RemoveModifierByName('modifier_creep_slow')
                                heroWithFlag = nil
                                CustomGameEventManager:Send_ServerToPlayer(hero:GetPlayerOwner(), "hellfire_flag_lost", {} )
                            end
                        else
                            hero:RemoveModifierByName('modifier_creep_slow')
                            heroWithFlag = nil
                            CustomGameEventManager:Send_ServerToPlayer(hero:GetPlayerOwner(), "hellfire_flag_lost", {} )
                        end
                    end
                end
            end

            if heroWithDireFlag then
                local hero = heroWithDireFlag
                if hero then
                    for i=0, 5 do
                        local item = hero:GetItemInSlot(i)
                        if item then
                            if  item:GetAbilityName() == 'item_capture_flag_dire' then
                                hero:AddNewModifier(hero, nil, 'modifier_creep_slow' ,nil)
                                heroWithDireFlag = hero
                                break
                            else
                                hero:RemoveModifierByName('modifier_creep_slow')
                                heroWithDireFlag = nil
                                CustomGameEventManager:Send_ServerToPlayer(hero:GetPlayerOwner(), "hellfire_flag_lost", {} )
                            end
                        else
                            hero:RemoveModifierByName('modifier_creep_slow')
                            heroWithDireFlag = nil
                            CustomGameEventManager:Send_ServerToPlayer(hero:GetPlayerOwner(), "hellfire_flag_lost", {} )
                        end
                    end
                end
            end

end

function HellfireGameMode:ItemPickedUp( playerID )
    local hero = PlayerResource:GetSelectedHeroEntity(playerID)
        if hero then
            for i=0, 5 do
                local item = hero:GetItemInSlot(i)
                if item then
                    if item:GetAbilityName() == 'item_capture_flag' then
                        if hero:GetTeam() == DOTA_TEAM_GOODGUYS then
                            UTIL_RemoveImmediate(item)
                            heroWithFlag = nil
                            spawnRadiantFlag()
                            break
                        else
                            hero:AddNewModifier(hero, nil, 'modifier_creep_slow' ,nil)
                            heroWithFlag = hero
                            CustomGameEventManager:Send_ServerToPlayer(hero:GetPlayerOwner(), "hellfire_flag_obtained", {} )
                            break
                        end
                    elseif item:GetAbilityName() == 'item_capture_flag_dire' then
                        if hero:GetTeam() == DOTA_TEAM_BADGUYS then
                            UTIL_RemoveImmediate(item)
                            heroWithDireFlag = nil
                            spawnDireFlag()
                            break
                        else
                            hero:AddNewModifier(hero, nil, 'modifier_creep_slow' ,nil)
                            heroWithDireFlag = hero
                            CustomGameEventManager:Send_ServerToPlayer(hero:GetPlayerOwner(), "hellfire_flag_obtained", {} )
                            break
                        end
                    end
                end
            end
        end
end