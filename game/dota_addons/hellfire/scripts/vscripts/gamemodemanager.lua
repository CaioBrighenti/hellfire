--Hopefully wil manage gamemodes
if GameModeManager == nil then
    _G.GameModeManager = class({})
    print('GameModeManager Loaded')
end

GameModeManager.CustomGameMode = {}
GameModeManager.GameModeLoaded = false

require('gamemodes/headhunter')

function GameModeManager:SetGameMode( string )

    require('gamemodes/' .. string)

    GameModeManager.CustomGameMode:Init()
    GameModeManager.CustomGameModeLoaded = true
end

function GameModeManager:RunGameMode()
    if GameModeManager.CustomGameModeLoaded == true then
        GameModeManager.CustomGameMode:think()
    end
end
