require('hellfire')

function Precache( context )
	--[[
		Precache things we know we'll use.  Possible file types include (but not limited to):
			PrecacheResource( "model", "*.vmdl", context )
			PrecacheResource( "soundfile", "*.vsndevts", context )
			PrecacheResource( "particle", "*.vpcf", context )
			PrecacheResource( "particle_folder", "particles/folder", context )
	]]

	PrecacheResource( "particle", "particles/arrow/arrow.vpcf", context )
	PrecacheResource( "particle", "particles/items_fx/blink_dagger_start.vpcf", context )
	PrecacheResource( "particle", "particles/items_fx/blink_dagger_end.vpcf", context )
	PrecacheResource( "particle", "particles/candy/candy_aura_tintable.vpcf", context )
	PrecacheResource( "particle", "particles/econ/items/faceless_void/faceless_void_jewel_of_aeons/phoenix_sunray_beam.vpcf", context )
	PrecacheResource( "particle", "particles/force_field/force_field_shell.vpcf", context )
	PrecacheResource( "particle", "particles/laser/laser_beam.vpcf", context )
	PrecacheResource( "particle", "particles/laser/laser_fire.vpcf", context )

	PrecacheResource( "model", "models/props_teams/banner_radiant.vmdl", context )
	PrecacheResource( "model", "models/props_teams/banner_dire_small.vmdl", context )
	PrecacheResource( "model", "models/particle/sphere.vmdl", context)

	PrecacheResource( "soundfile", "soundevents/game_sounds_heroes/game_sounds_ember_spirit.vsndevts", context)

end

-- Create the game mode when we activate
function Activate()
	GameRules.Hellfire = HellfireGameMode()
	GameRules.Hellfire:InitGameMode()
end
