--[[
Hellfire Gamemode
by: Critwhale and Kobb
]]--

---------------------------------------------------------------------------
-- Hellfire Gamemode Class
---------------------------------------------------------------------------

if HellfireGameMode == nil then
	_G.HellfireGameMode = class({})
end

-- Constants
	LOOPTIME = 0.1

-- Libraries
require('libraries/projectiles')
require('events')
--require('libraries/flag')
require('gamemodemanager')

function HellfireGameMode:InitGameMode()
	print( "[ HELLFIRE INITIALIZED ]" )

	-- Variables
	HellfireGameMode:InitColors()
	-- GameRules

	GameRules:SetCustomGameTeamMaxPlayers(2, 2)
	GameRules:SetCustomGameTeamMaxPlayers(3, 2)
	GameRules:SetCustomGameTeamMaxPlayers(6, 2)
	GameRules:SetCustomGameTeamMaxPlayers(7, 2)
	GameRules:SetCustomGameTeamMaxPlayers(8, 2)
	GameRules:SetCustomGameTeamMaxPlayers(9, 2)
	GameRules:SetCustomGameTeamMaxPlayers(10, 2)
	GameRules:SetCustomGameTeamMaxPlayers(11, 2)
	GameRules:SetCustomGameTeamMaxPlayers(12, 2)
	GameRules:SetCustomGameTeamMaxPlayers(13, 2)

	GameRules:SetPreGameTime( 1 )
	GameRules:SetPostGameTime( 30 )

	CustomNetTables:SetTableValue( "TeamScore", tostring(2), { score = 0 })
	CustomNetTables:SetTableValue( "TeamScore", tostring(3), { score = 0 })
	CustomNetTables:SetTableValue( "TeamScore", tostring(6), { score = 0 })
	CustomNetTables:SetTableValue( "TeamScore", tostring(7), { score = 0 })
	CustomNetTables:SetTableValue( "TeamScore", tostring(8), { score = 0 })
	CustomNetTables:SetTableValue( "TeamScore", tostring(9), { score = 0 })
	CustomNetTables:SetTableValue( "TeamScore", tostring(10), { score = 0 })
	CustomNetTables:SetTableValue( "TeamScore", tostring(11), { score = 0 })
	CustomNetTables:SetTableValue( "TeamScore", tostring(12), { score = 0 })
	CustomNetTables:SetTableValue( "TeamScore", tostring(13), { score = 0 })

	--CustomNetTables:SetTableValue( "HF_PlayerData", tostring(2), { score = 0 })

	-- GameMode
	GameRules:GetGameModeEntity():SetLoseGoldOnDeath( false )
	GameRules:GetGameModeEntity():SetFixedRespawnTime( 5 )
	--GameRules:GetGameModeEntity():SetFogOfWarDisabled( true )
	GameRules:GetGameModeEntity():SetModifyGoldFilter( Dynamic_Wrap( HellfireGameMode, "ModifyGoldFilter" ), self )
	GameRules:SetSameHeroSelectionEnabled( true )

	-- Init Modifiers
	LinkLuaModifier( "modifier_force_field", "abilities/modifier_force_field", LUA_MODIFIER_MOTION_NONE )
	LinkLuaModifier( "modifier_player_modifiers", "abilities/modifier_player_modifiers", LUA_MODIFIER_MOTION_NONE )
	LinkLuaModifier( "modifier_boost", "abilities/modifier_boost", LUA_MODIFIER_MOTION_NONE )

	-- Map Entities

	-- Init ThinkState
	HellfireGameMode.thinkState = Dynamic_Wrap( HellfireGameMode, '_thinkState_PreGame' )

	-- Events
	ListenToGameEvent('npc_spawned', Dynamic_Wrap(HellfireGameMode, 'OnNPCSpawned'), self)
	ListenToGameEvent('dota_item_picked_up', Dynamic_Wrap(HellfireGameMode, 'OnItemPickedUp'), self)
	ListenToGameEvent('game_rules_state_change', Dynamic_Wrap(HellfireGameMode, 'OnStateChange'), self)

	CustomGameEventManager:RegisterListener('hellfire_vote_update', Dynamic_Wrap(HellfireGameMode, 'OnVoteUpdate'))

	-- Think Function
	GameRules:GetGameModeEntity():SetThink( "OnThink", HellfireGameMode, LOOPTIME )


	-- Set up flag stuff
--	spawnDireFlag()
--    spawnRadiantFlag()
--    heroWithFlag = nil
--    heroWithDireFlag = nil

	print( "[ HELLFIRE LOADED ]" )
end

---------------------------------------------------------------------------
-- Think Function
---------------------------------------------------------------------------

function HellfireGameMode:OnThink()
	for nPlayerID = 0, (DOTA_MAX_TEAM_PLAYERS-1) do
		self:UpdatePlayerColor( nPlayerID )
	end

	-- Pauses would fuck the timer otherwise.
	if GameRules:IsGamePaused() == true then
		return LOOPTIME
	end

	--HellfireGameMode:UpdateFlags()
	GameModeManager:RunGameMode()

	return LOOPTIME
end

---------------------------------------------------------------------------
-- Put a label over a player's hero so people know who is on what team
---------------------------------------------------------------------------
function HellfireGameMode:UpdatePlayerColor( nPlayerID )
	if not PlayerResource:HasSelectedHero( nPlayerID ) then
		return
	end

	local hero = PlayerResource:GetSelectedHeroEntity( nPlayerID )
	if hero == nil then
		return
	end

	local teamID = PlayerResource:GetTeam( nPlayerID )
	local color = self:ColorForTeam( teamID )
	PlayerResource:SetCustomPlayerColor( nPlayerID, color[1], color[2], color[3] )
end

---------------------------------------------------------------------------
-- Get the color associated with a given teamID -- From Overthrow
---------------------------------------------------------------------------
function HellfireGameMode:ColorForTeam( teamID )
	local color = HellfireGameMode.Colors[ teamID ]
	if color == nil then
		color = { 255, 255, 255 } -- default to white
	end
	return color
end

function HellfireGameMode:ModifyGoldFilter( filterTable )
	filterTable["gold"] = 0
	return true
end

function HellfireGameMode:InitColors()

	colors = {}
	colors[DOTA_TEAM_GOODGUYS] = { 255, 0, 0 } -- Indigo
	colors[DOTA_TEAM_BADGUYS]  = { 255, 153, 0 } -- Red
	colors[DOTA_TEAM_CUSTOM_1] = { 204, 255, 0 }
	colors[DOTA_TEAM_CUSTOM_2] = { 51, 255, 0 }
	colors[DOTA_TEAM_CUSTOM_3] = { 0, 255, 102 }
	colors[DOTA_TEAM_CUSTOM_4] = { 0, 255, 255 }
	colors[DOTA_TEAM_CUSTOM_5] = { 0, 102, 255 }
	colors[DOTA_TEAM_CUSTOM_6] = { 51, 0, 255 }
	colors[DOTA_TEAM_CUSTOM_7] = { 204, 0, 255 }
	colors[DOTA_TEAM_CUSTOM_8] = { 255, 0, 153 }

	HellfireGameMode.Colors = colors

	for team = 0, (DOTA_TEAM_COUNT-1) do
		color = HellfireGameMode.Colors[ team ]
		if color then
			SetTeamCustomHealthbarColor( team, color[1], color[2], color[3] )
		end
	end
end
