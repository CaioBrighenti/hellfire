"DOTAAbilities"
{
	//=================================================================================================================
	// Recipe: Arcane Boots
	//=================================================================================================================
	"item_recipe_arcane_boots2"
	{
		"ID"							"1005"

		"BaseClass"						"item_recipe_arcane_boots"

		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemCost"						"0"
		"ItemShopTags"					""

		// Recipe
		//-------------------------------------------------------------------------------------------------------------
		"ItemRecipe"					"1"
		"ItemResult"					"item_arcane_boots2"
		"ItemRequirements"
		{
			"01"						"item_energy_booster;item_arcane_boots"
		}
	}

	//=================================================================================================================
	// Arcane Boots II
	//=================================================================================================================
	"item_arcane_boots2"
	{
		"ID"							"1006"

		// General
		//-------------------------------------------------------------------------------------------------------------
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_IMMEDIATE | DOTA_ABILITY_BEHAVIOR_NO_TARGET"
		"BaseClass"						"item_arcane_boots"

		// Stats
		//-------------------------------------------------------------------------------------------------------------
		"AbilityCooldown"				"55.0"

		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"AbilityManaCost"				"25"
		"ItemCost"						"2450"
		"ItemShopTags"					"move_speed;boost_mana;mana_pool"
		"ItemQuality"					"rare"
		"ItemDeclarations"				"DECLARE_PURCHASES_TO_SPECTATORS"
		"ItemDisassembleRule"			"0"
		"ItemAlertable"					"1"
		"ItemAliases"					"mana;mb"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_movement"		"75"
			}
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_mana"			"500"
			}
			"03"
			{
				"var_type"				"FIELD_INTEGER"
				"replenish_radius"		"600"
			}
			"04"
			{
				"var_type"				"FIELD_INTEGER"
				"replenish_amount"		"250"
			}
		}
	}

	"item_capture_flag"
	{
		"BaseClass"						"item_datadriven"
		"AbilityTextureName"			"capture_flag"
		// General
		//-------------------------------------------------------------------------------------------------------------
		"ID"							"1334"														// unique ID number for this item.  Do not change this once established or it will invalidate collected stats.
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE"
		"Model"							"models\props_teams\banner_radiant.mdl"
		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemCost"						"999999"
		"ItemShopTags"					"damage"
		"ItemDroppable"					"1"
		"ItemQuality"					"epic"
		"ItemShareability"				"ITEM_FULLY_SHAREABLE"
		"ItemSellable"					"0"
		"ItemKillable"					"0"
		"ItemDeclarations"				"DECLARE_PURCHASES_TO_TEAMMATES | DECLARE_PURCHASES_TO_SPECTATORS | DECLARE_PURCHASES_IN_SPEECH"
	}

	"item_capture_flag_dire"
	{
		"BaseClass"						"item_datadriven"
		"AbilityTextureName"			"capture_flag_dire"
		// General
		//-------------------------------------------------------------------------------------------------------------
		"ID"							"1335"														// unique ID number for this item.  Do not change this once established or it will invalidate collected stats.
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT_TARGET"
		"Model"							"models\props_teams\banner_dire_small.mdl"
		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemCost"						"999999"
		"ItemShopTags"					"damage"
		"ItemDroppable"					"1"
		"ItemQuality"					"epic"
		"ItemShareability"				"ITEM_FULLY_SHAREABLE"
		"ItemSellable"					"0"
		"ItemKillable"					"0"
		"ItemDeclarations"				"DECLARE_PURCHASES_TO_TEAMMATES | DECLARE_PURCHASES_TO_SPECTATORS | DECLARE_PURCHASES_IN_SPEECH"

		"OnEquip"
		{
			"RunScript"
			{
				"ScriptFile"					"scripts/vscripts/items/flag_dire.lua"
				"Function"						"Equip"
			}
		}

		"OnUnEquip"
		{
			"RunScript"
			{
				"ScriptFile"					"scripts/vscripts/items/flag_dire.lua"
				"Function"						"Unequip"
			}
		}
	}

	"item_candy"
	{
		"BaseClass"						"item_datadriven"
		"AbilityTextureName"			"capture_flag_dire"
		// General
		//-------------------------------------------------------------------------------------------------------------
		"ID"							"1336"														// unique ID number for this item.  Do not change this once established or it will invalidate collected stats.
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE"
		"Model"							"models/props_gameplay/halloween_candy.vmdl"
		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemCost"						"999999"
		"ItemShopTags"					"damage"
		"ItemDroppable"					"1"
		"ItemQuality"					"epic"
		"ItemShareability"				"ITEM_FULLY_SHAREABLE"
		"ItemSellable"					"0"
		"ItemKillable"					"0"
		"ItemDeclarations"				"DECLARE_PURCHASES_TO_TEAMMATES | DECLARE_PURCHASES_TO_SPECTATORS | DECLARE_PURCHASES_IN_SPEECH"

		"OnEquip"
		{
			"RunScript"
			{
				"ScriptFile"					"scripts/vscripts/items/candy.lua"
				"Function"						"Equip"
			}
		}
	}
}
